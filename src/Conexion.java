import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexion {
	String URL_DB="jdbc:postgresql://localhost:5432/";
	String USER_DB="postgres";
	String NAME_DB="postgres";
	String PASS_DB="root";
	Connection con=null;
	private static Conexion conexion;
	public Conexion() throws SQLException, ClassNotFoundException{
		Class.forName("org.postgresql.Driver");
		 con=DriverManager.getConnection(URL_DB+NAME_DB,USER_DB,PASS_DB);
		 if	(con!=null){
			System.out.println("conexion exitosa..!!!");
		 }else{
		 System.out.println("error en la conexion .!!!");
		 }	
	}
	
 public static Conexion getInstancia() throws ClassNotFoundException, SQLException{
	 if	(conexion==null){
		 conexion=new Conexion();
	 }
	 return conexion;
 }
 public Connection getConexion(){
	 return con;
 }

}

