import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



public class Usuario {
	Conexion con;
	int id;
	String nombre;
	String correo;
	String contrasena;
	int tipo;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}

	public Usuario(){
		try {
			con=Conexion.getInstancia();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Usuario(int r_id, String r_nombre, String r_correo,
			String r_correo2, String r_contrasena, int r_tipo) {
		setNombre(r_nombre);
		setTipo(r_tipo);
		setId(r_id);
		setCorreo(r_correo2);
		setContrasena(r_contrasena);
	}

	public boolean registrar() throws ClassNotFoundException, SQLException{
		Connection conection=con.getConexion();
		Statement st;
			st = conection.createStatement();
		String sql="INSERT INTO public.usuario(nombre, correo, contrasena, tipo) VALUES ('"+getNombre()+"','"+getCorreo()+"','"+getContrasena()+"',"+getTipo()+");";
		int res=st.executeUpdate(sql);
		st.close();
		if (res==1) {
			System.out.println("registro exitoso");
			return true;
		}else{
			System.out.println("registro fallido");
			return false;
		}		
		
	}
	public boolean editar() throws SQLException{
		Connection conection=con.getConexion();
		Statement st;
		st = conection.createStatement();
		String sql="UPDATE usuario set nombre='"+getNombre()+"', correo='"+getCorreo()+"', contrasena='"+getContrasena()+"' "; 
		sql=sql+"where id="+getId()+";";
		int res=st.executeUpdate(sql);
		st.close();
		if (res==1) {
			System.out.println("ACtualizacion exitoso");
			return true;
		}else{
			System.out.println("Actualizacion fallido");
			return false;
		}
	}
	
	public boolean eliminar() throws SQLException {
		Connection conection=con.getConexion();
		Statement st;
			st = conection.createStatement();
		String sql="DELETE from USUARIO where id = "+getId()+";";
		int res=st.executeUpdate(sql);
		st.close();
		if (res==1) {
			System.out.println("ELIMINACION exitoso");
			return true;
		}else{
			System.out.println("ELIMINACION fallido");
			return false;
		}		
	}
	
	public List<Usuario> getUsuarios() throws SQLException{
		Connection conection=con.getConexion();
		Statement st;
			st = conection.createStatement();
		String sql="SELECT * FROM usuario;";
		ResultSet rs=st.executeQuery(sql);
		List<Usuario> lista=new ArrayList<Usuario>();
		while ( rs.next() ) {
			Usuario ur=new Usuario();
            ur.setId( rs.getInt("id"));
            ur.setNombre(rs.getString("nombre"));
            ur.setCorreo( rs.getString("correo"));
            ur.setContrasena( rs.getString("contrasena"));
            ur.setTipo(rs.getInt("tipo"));
            lista.add(ur);
         }
		return lista;
			
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getContrasena() {
		return contrasena;
	}
	public void setContrasena(String contrasena) {
		this.contrasena = contrasena;
	}
	public static void main(String[] args){
		 Usuario us=new Usuario();
		 try {
			//registrar usuario
			 us.setNombre("luis");
			 us.setCorreo("luis@gmail.com");
			 us.setContrasena("admin");
			 us.setTipo(1);
			 us.registrar();

			//Mostrar todos los usuarios
			int nro=us.getUsuarios().size();
			for (int i = 0; i <nro; i++) {
				Usuario usu=us.getUsuarios().get(i);
				System.out.println("id :"+usu.getId());
				System.out.println("nombre :"+usu.getNombre());
				System.out.println("correo :"+usu.getCorreo());
				System.out.println("contrasena :"+usu.getContrasena());
				System.out.println("tipo :"+usu.getTipo()+"\n");
				
			}
		
			///eliminar usuario
			us.setId(8);///poner el id del usuario a eliminar
			us.eliminar();
		
			
			//actualizar usuario
			us.setId(5);
			us.setNombre("felipe");
			us.setContrasena("54321");
			us.setCorreo("felipe@gamil.com");
			us.setTipo(2);
			us.editar();
			
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	 }
	
	
}
